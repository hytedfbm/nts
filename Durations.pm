package Durations;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = qw(dur bpm2sec);

sub dur{
	my ($c, $ppq) = @_;
	$ppq = ($ppq>1) ? $ppq : 1;	# 48, 96, ...
	my $d = ($c =~ /^(\d+)/) ? $1 : 1;


	if($c =~ /\/(\d+)/){	# 3/4
		$d = $d / $1;
	}
	elsif($c =~ /(\/+)/){	# ///
		$d = $d / (2 ** length($1));
	}
	elsif($c =~ /(\.+)/){	# dotted (. == 3/2)  (.. == 7/4)
		my $d2 = 2 ** length($1);
		$d = $d * ((2*$d2-1)/$d2);
	}
	elsif($c =~ /\:(\d+)/){	# tuplet 3:2
		$d = 1/$d;  #$1 / 1; #$d /2; # ???
	}
	
	return $d * $ppq;
}

#bpm to ms per beat
sub bpm2sec{
	my ($c) = @_;
	return 60/$c
}

1;
