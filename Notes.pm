package Notes;
use strict;
use Exporter;

our $VERSION = 1.00;
our @ISA     = qw(Exporter);
our @EXPORT  = qw(let2n n2let let2midi midi2let
	scaled getkey nts2abc);

my %sc = (C=>0, D=>2, E=>4, F=>5, G=>7, A=>9, B=>11);
my %ac = ('bb'=>-2, 'b'=>-1, '='=>0, '#'=>1, '##'=>2);

my @nt5 = qw(
	Fb Cb Gb Db Ab Eb Bb F
	C G D A E B F# C# G# D# A# E# B#);

################	note without oct
sub let2n{
    my ($c) = @_;

    my $n = let2midi($c);
    return (defined $n) ? $n % 12 : undef;
}

################
sub n2let{
    my ($n, $fs) = @_;
    
    if($n =~ /^\d+/){
        my $c = midi2let($n, $fs);
        $c =~ s/\-?\d+//;
        return $c;
    }
    else{ return undef; }
}

################
sub let2midi{
    my ($c, $o) = @_;
    
    if($c =~ /^[A-G][b\#\=]*(\-?\d)/i){ $o += $1; }
    elsif(! defined $o){ $o = -1; }
    
    if($c =~ /^([A-G])([b\#\=]*)/i){
        return ($sc{uc($1)} + $ac{$2}) + $o * 12 + 12;
    }
    else{ return undef; }
}

################
sub midi2let{
    my ($n, $fs) = @_;
    
    if($n =~ /^\d+/){
        my $o = int($n/12)-1;
        my $n12 = $n % 12;

        my %rsc = reverse %sc;
        my $c = $rsc{$n12};
        if(! defined $c){
            $c = $fs ? $rsc{($n12-1)%12} . '#'
                     : $rsc{($n12+1)%12} . 'b';
        }
        return $c . $o;
    }
    else{ return undef; }
}

################
sub scaled{
    my ($c, $k) = @_;
    
    if($c =~ /([A-G][b\#\=]+.*)/i){ return ucfirst($1); }
    elsif($c =~ /([A-G])(.*)/i){ 
        if(ref($k) ne 'HASH'){ ($k) = getkey($k); }
        return	$1 . $k->{uc($1)} . $2;
    }
    else{ return undef; }
}

################
sub getkey{
    my ($ks, @a0) = @_;

    my $nfs;	 #get number of flats or sharps
	if($ks =~ /^\-?\d$/ && $ks>=-7 && $ks<=7){ 	# -7 to 7
		$nfs = $ks;
	}
	elsif($ks =~ /^([A-G][b\#]?)(m?)$/){ 
		my $x0 = $2 eq 'm' ? 4  # MIN	Ab..A#
						   : 1; # MAJ	Cb..C#
		$ks = $1;
		$nfs = -7; 
		for (@nt5[$x0..14+$x0]){
			if($ks eq $_){last}
			$nfs++;
		}
	}

	my @a1;
	if(defined $nfs){
		#get the modified notes
		my @nn = @nt5;
		(@a1) = ($nfs>=0)	? splice(@nn,14,$nfs) 			#sharps
							: splice(@nn,7+$nfs,-1*$nfs);	#flats
	}
	push @a1, @a0;

	#make hash  { notebase => accent }
	my %a2 = map { /([A-G])([b\#]*)/; } @a1;
	
	return (\%a2, \@a1, $nfs);
}

################
sub nts2abc{
	my (@nts) = @_;
	my @abc;

	for(@nts){
		if(/^([A-G])([b=#]*)(\-?\d)/i){
			my ($n,$ac,$o) = ($1,$2,$3);
			$ac =~ tr/b=#/_=^/;
			$o -= 4;
			$n = ($o>0) ? lc($n) : uc($n);
			my $o2 = ($o>0) ? "'" x ($o-1) : "," x ($o * -1);
			push @abc, $ac . $n . $o2;
		}
	}

	return @abc;
}

1;