package NotesDr;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = qw(dr2midi midi2dr dr2staff);


my %drmap = (
	K=>{ MIDI=>[36,35], STAFF=>['E4'] }, 		#KICK
	S=>{ MIDI=>[38,40], STAFF=>['C5'] },		#SNARE
	X=>{ MIDI=>[39,31],	STAFF=>['C5', '/'] },
	T=>{ MIDI=>[47,45,48,50], STAFF=>['E5'] },	#TOMS
	F=>{ MIDI=>[43,41], STAFF=>['A4'] },
	H=>{ MIDI=>[42], STAFF=>['G5', 'x'] },			#HI HAT
	P=>{ MIDI=>[44], STAFF=>['D4', 'x'] },
 	O=>{ MIDI=>[46], STAFF=>['G5', 'xo'] },
										# non-std midi  
										# half-open=23, splash=21
										# edge=22, open-edge=26
										# ped=cc4

	C=>{ MIDI=>[49,57,52,55], STAFF=>['A5 x'] },#CRASH
	R=>{ MIDI=>[51,59,53], STAFF=>['F5 x'] },	#RIDE
												#TAMB 54 	B4
												#COWB 56	E5
);

sub dr2midi{
	my ($c) = @_;
	my ($c1) = $c =~ /([A-Y])/i;
	my $n = $drmap{uc($c1)};

	if(defined $n){
		return $n->{MIDI}[0];
	}
	else{ return undef; }
}

sub midi2dr{
	my ($n) = @_;
	
	if($n =~ /^\d+$/){
		for my $dr(keys %drmap){
			for my $m( @{$drmap{$dr}{MIDI}} ){
				if($m == $n){ return $dr; }
			}
		}
	}
	return undef;
}


sub dr2staff{
	my ($c) = @_;
	my ($c1) = $c =~ /([A-Y])/i;
	my $n = $drmap{uc($c1)};

	if(defined $n){
		return @{$n->{STAFF}};  # [pos, head]
	}
	else{ return undef; }
	
}

1;