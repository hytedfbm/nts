package Tabs;
use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = qw(tuning gettuning fret2note fretdiag2notes);

use Notes;

my %tunings = (
	#GTR		#score = +1 oct
	STD    => 'EADGBE',
	EADGBE => 'E2A2D3G3B3E4',

	DROPD  => 'DADGBE',

	NASH   => 'eadgBE',
	'12STR' => 'NASH',

	#DOBRO		#score = +1 oct
	DOBRO  => 'GBDGBD',

	#BANJO		#score = +1 oct
	BANJO  => 'OPENG',
	OPENG  => 'gDGBD',

	DBL_C  => 'gCGCD',
	DBL_D  => 'DBL_C+2',
	SAWMILL=> 'gDGCD',

	#   PLECTRUM
	BANJO4 => 'CGBD',
	#   TENOR
	BANJOT => 'CGDA',

	#BASS		#score = +1 oct
	BASS   => '1EADG',

	#MANDO		#score = no trans
	MANDO  => '3GDAE',

);

###########
sub tuning{
	my ($a, $capo) = @_;
	if($a =~ s/\+(\-?\d)$//){ $capo += $1; }

	while(exists $tunings{$a}){
		$a = $tunings{$a};
		if($a =~ s/\+(\-?\d)$//){ $capo += $1; }
	}
	my (@t) = $a =~ /([A-G][#b]?\-?\d+)/g;
	my (@t2) = $a =~ /([A-G][#b]?)/g;

	if(@t==0 || @t != @t2){ (@t) = tuningguess($a); }

	if($capo!=0){ (@t) = capo(\@t, $capo, 1); }

	return @t;
}
sub capo{
	my ($t, $capo, $fs) = @_;

	my @t2 = map { midi2let( let2midi($_)+$capo, $fs); } @$t;
	return @t2;
}
sub tuningguess{
	my ($a, $o) = @_;
	my (@t) = $a =~ /([A-Ga-g][#b]?\d*)/g;

	if(!defined $o){	#octave 
		if($a =~ /^(\-?\d+)/){ $o = $1; } #has default
		elsif(@t==5 || @t==4){ $o = 3; } #banjo
		else{ $o = 2; } #default guitar
	}
	
	my $last = -1;
	(@t) = map {
		if(/([A-Ga-g][#b]?)(\d+)/){ $_ = $1; $o = $2; $last = -1;  }
		my $n = let2n($_);
		if($last>0 && $n<=$last){ $o++; }
		$last = $n;

		my $o2 = ($_ eq lc($_)); #lc notes are 1 oct higher
		if($o2){ $last = -1; }
		uc($_) . ($o+$o2);
	} @t;

	return @t;
}

##############
sub gettuning{
	my ($a, $capo) = @_;
	my $str12 = $a =~ s/\s*12STR//i;

	my @t = tuning($a);
	if(defined $capo){ (@t) = capo(\@t,$capo,1); }
	my @nts = map { let2midi($_); } reverse @t;
	
	my $t = { TUNING=>join('',@t), NTS=>\@nts, NSTR=>0+@nts, }; 
	if($capo != 0){ $t->{CAPO} = $capo; }
	if(@t==5){ 		$t->{BANJO} = 1; }
	if($str12){		$t->{STR12} = 1; }

	return $t;
}

##############
sub fret2note{
	my ($f,$s, $t, $fs) = @_;
	my ($n0, $nt);

	if(exists $t->{NTS}[$s]){
		$n0 = $t->{NTS}[$s];
	}

	#BANJO DRONE  5th str
	if($t->{BANJO} && $s==4){
		if($f==0){ }
		elsif($f<5){ $f = ''; }
		else{ $f = $f - 5; }
	}

	#12 string octaves
	my $octs = ($t->{STR12} && $s>=2);


	if(defined $n0 && $f =~ /(\d+)/){
		$nt = midi2let($n0+$1, $fs);
		if($octs && $nt =~ /(.+)(\-?\d+)/){ $nt .= $1 . ($2+1); }
	}
	return $nt;
}

###################
sub fretdiag2notes{
	my ($fd, $t, $fs) = @_;
	
	my @f = ($fd) =~ /(\s1?\d|\d|[zx-])/g;
	(@f) = reverse @f;

	my (@nts) = map { fret2note($f[$_], $_, $t, $fs); } (0..@f-1);
	(@nts) = reverse @nts;

	return @nts;
}

1;